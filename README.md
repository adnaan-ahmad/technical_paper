## 1. MongoDB 

* It's a Document-oriented database 

* Stores data similar to JSON

* Used for dynamic applications

* Ease of scale-out − It is easy to scale

* No complex joins

## 2. Apache Cassandra

* It's a Column-oriented Database 

* Reads and writes data in columns instead of rows 

* Good for Analytics

* Supports ACID properties of a transaction

* Can store hundreds of terabytes of data

## 3. Redis 

* It stores data in the form of key-value store

* It's optimal for huge dataset

* Extremely fast and simplest database

* Supports datatypes that developers already know such as list, set, hash, etc

## 4. Memcache 

* It's a Cache system database 

* Can be used as cache system for temporary values

* Stores data in the form of key-value pair like redis

* It's open source

## 5. Neo4J 

* It's a Graph-oriented database 

* Stores data in the form of nodes(vertices). Nodes have relation(edges) between them

* Used for huge datasets like social networks

* Provides flexible data model which can be easily changed according to the applications and industries 

### References -

#### [Traversy Media Youtube Video](https://www.youtube.com/watch?v=uD3p_rZPBUQ)

#### [Tutorials Point](https://www.tutorialspoint.com/index.htm)
